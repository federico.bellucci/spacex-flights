A simple React application that consumes data from the [SpaceX](https://docs.spacexdata.com) public API. 

# Instructions
To install run :
```
npm install
```

To start the application run:
```
npm start
```
A web server will be served on `http://localhost:3000`

To run unit tests: 
```
npm run test
```

To produce the test coverage: 
```
npm run coverage
```
Results can be viewed on the browser opening the file `coverage/lcov-report/index.html`

# Notes
- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- The application is mobile first, but on some devices (simulated on Chrome developer tools), such as iPhone or Samsung Galaxy, the content width will be automaitcally scaled and the application will be dodgy. On Pixel2 there is no such issue. 
- Tests have been only partially implemented.
 
