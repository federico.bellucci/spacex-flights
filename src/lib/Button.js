import React from 'react'
import './Button.scss'

export default ({
  icon, className, onClick, children
}) => {
  const classes = ['button']
  if (className) {
    classes.push(className)
  }

  return (
    <button onClick={onClick} className={classes.join(' ')}>
      {children}
      {icon ? <i className={icon}>{icon}</i> : null}
    </button>
  )
}
