import React, { useState } from 'react'
import Option from './Option'
import Button from './Button'
import './Select.scss'

export default ({
  options, value, label, onSelect
}) => {
  const [open, setOpen] = useState(false)

  const onOpen = () => setOpen(!open)

  const onOptionSelect = (selected) => {
    onSelect(selected)
    setOpen(!open)
  }
  return (
    <div className="collapsible">
      <Button className="collapsible-toggle" onClick={onOpen} icon="filter">
        {label}
        {value && ` : ${value}`}
      </Button>
      <div className={`collapsible-content${open ? ' open' : ''}`}>
        <ul className="content-inner">
          <Option key="none" value={null} text="None" onSelect={onOptionSelect} />
          {options.map((option) => <Option key={option} value={option} onSelect={onOptionSelect} />)}
        </ul>
      </div>
    </div>
  )
}
