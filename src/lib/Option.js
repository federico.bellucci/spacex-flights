import React from 'react'
import './Option.scss'

export default ({ value, text, onSelect }) => (
  <li
    className="item"
    onClick={() => onSelect(value)}
  >
    {text || value}
  </li>
)
