import React from 'react'
import './Header.scss'
import Button from '../lib/Button'

export default ({ refresh }) => (
  <header className="app-header">
    <div className="logo">
      <div className="logo-img" />
      <div className="logo-text">Launches</div>
    </div>
    <div className="reload-container">
      <Button onClick={refresh} icon="refresh">Reload Data</Button>
    </div>
  </header>
)
