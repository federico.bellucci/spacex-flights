import axios from 'axios'
import * as api from './api'
import { getLaunches, getYears } from '.'

jest.mock('axios')
axios.CancelToken.source.mockImplementation(() => ({
  token: jest.fn(),
  cancel: jest.fn()
}))

describe('api', () => {
  describe('getLaunches', () => {
    beforeEach(() => {
      api.parseParameters = jest.fn()
      api.processLaunches = jest.fn()
      axios.get.mockResolvedValue({ data: [] })
      return getLaunches({})
    })

    test('should call Axios.get', async () => {
      expect(axios.get)
        .toHaveBeenCalledWith(
          expect.stringMatching(/\/launches/),
          expect.any(Object)
        )
    })

    test('should call parseLaunches', async () => {
      expect(api.parseParameters).toHaveBeenCalled()
    })

    test('should call processLaunches', async () => {
      expect(api.processLaunches).toHaveBeenCalled()
    })
  })

  describe('getYears', () => {
    beforeEach(() => {
      api.processYears = jest.fn()
      axios.get.mockResolvedValue({ data: [] })
      return getYears()
    })

    test('should call Axios.get', async () => {
      expect(axios.get)
        .toHaveBeenCalledWith(
          expect.stringMatching(/\/launches/),
          expect.any(Object)
        )
    })

    test('should call processYears', async () => {
      expect(api.processYears).toHaveBeenCalled()
    })
  })
})
