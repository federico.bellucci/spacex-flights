import moment from 'moment'

export const parseParameters = ({ year, sort }) => {
  const params = {
    filter: [
      'flight_number',
      'mission_name',
      'launch_date_utc',
      'rocket/rocket_name'
    ].join(',')
  }

  if (year) {
    params.launch_year = year
  }

  if (sort) {
    params.sort = 'launch_date_utc'
    params.order = sort
  }

  return params
}

export const processLaunches = (data) => data.map(({
  flight_number,
  mission_name,
  rocket: { rocket_name },
  launch_date_utc
}) => ({
  id: flight_number,
  missionName: mission_name,
  rocketName: rocket_name,
  date: moment(launch_date_utc).format('Do MMM YYYY')
}))

export const processYears = (data) => {
  const set = new Set()
  data.forEach(({ launch_year }) => {
    set.add(launch_year)
  })
  return Array.from(set).sort()
}
