import axios from 'axios'
import { parseParameters, processLaunches, processYears } from './api'

const BASE_URL = 'https://api.spacexdata.com/v3'
const { CancelToken } = axios

let sourceLaunches
let sourceYears

export const getLaunches = async (params) => {
  sourceLaunches && sourceLaunches.cancel('Operation canceled by the user.')
  sourceLaunches = CancelToken.source()

  const { data } = await axios.get(`${BASE_URL}/launches`, {
    params: parseParameters(params),
    cancelToken: sourceLaunches.token
  })
  return processLaunches(data)
}

export const getYears = async () => {
  sourceYears && sourceYears.cancel('Operation canceled by the user.')
  sourceYears = CancelToken.source()

  const { data } = await axios.get(`${BASE_URL}/launches`, {
    params: { filter: 'launch_year' },
    cancelToken: sourceYears.token
  })
  return processYears(data)
}
