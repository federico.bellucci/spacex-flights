import { parseParameters, processLaunches, processYears } from './api'

describe('api', () => {
  describe('parseParameters', () => {
    const BASE_PARAMS = {
      filter: [
        'flight_number',
        'mission_name',
        'launch_date_utc',
        'rocket/rocket_name'
      ].join(',')
    }

    test('with no args should return the base parameters', () => {
      expect(parseParameters({})).toStrictEqual(BASE_PARAMS)
    })

    describe('with args', () => {
      beforeEach(() => {
        expect(parseParameters({})).toEqual(expect.objectContaining(BASE_PARAMS))
      })

      test('"year" should return object containg "launch_year"', () => {
        expect(parseParameters({ year: 2020 })).toHaveProperty('launch_year', 2020)
      })

      test('"sort" should return object containg "sort"', () => {
        expect(parseParameters({ sort: 'asc' })).toHaveProperty('sort', 'launch_date_utc')
        expect(parseParameters({ sort: 'asc' })).toHaveProperty('order', 'asc')
      })
    })
  })

  describe('processLaunches', () => {
    const launches = [
      {
        flight_number: 1,
        mission_name: 'FalconSat',
        rocket: { rocket_name: 'Falcon 1' },
        launch_date_utc: '2006-03-24T22:30:00.000Z'
      }
    ]
    let result

    beforeEach(() => {
      result = processLaunches(launches)
    })

    test('should return an array', () => {
      expect(Array.isArray(result)).toBe(true)
    })

    test('result array should containin an object', () => {
      expect(result).toContainEqual({
        id: 1,
        missionName: 'FalconSat',
        rocketName: 'Falcon 1',
        date: '24th Mar 2006'
      })
    })
  })

  describe('processYears', () => {
    const years = [
      { launch_year: 2008 },
      { launch_year: 2006 },
      { launch_year: 2006 },
      { launch_year: 2009 }
    ]

    let result

    beforeEach(() => {
      result = processYears(years)
    })

    test('should return an array', () => {
      expect(Array.isArray(result)).toBe(true)
    })

    test('result array should contain unique items', () => {
      expect(result).toBeDistinct()
    })

    test('result array should be sorted', () => {
      expect(result).toEqual([
        2006,
        2008,
        2009
      ])
    })
  })
})
