// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect'

/**
 * @link https://stackoverflow.com/a/57002833/274424
 */
expect.extend({
  toBeDistinct (received) {
    const pass = Array.isArray(received) &&
      new Set(received).size === received.length
    if (pass) {
      return {
        message: () => `expected [${received}] array is unique`,
        pass: true
      }
    }
    return {
      message: () => `expected [${received}] array is not to unique`,
      pass: false
    }
  }
})
