import React from 'react'
import Launch from './Launch'
import './Launches.scss'
import { LOADING, ERROR } from './status'
import Actions from './Actions'

export default ({ data: { launches, years }, status, ...actionProps }) => (
  <div className="main">
    <Actions
      years={years}
      {...actionProps}
    />
    <div className="container">
      <div className="background-art" />
      <div className="content">
        {
          status === ERROR &&
          <div className="error">Error loading data. Please try again later</div>
        }
        {
          status === LOADING &&
          <div className="loading">Loading...</div>
        }
        <ul className="list">
          {launches.map((launch) => <Launch key={launch.id} item={launch} />)}
        </ul>
      </div>
    </div>
  </div>
)
