import React from 'react'
import './Actions.scss'
import Select from '../lib/Select'
import Button from '../lib/Button'

export default ({
  years, sort, year, onYearSelect, onSortChange
}) => (
  <div className="actions">
    <Select value={year} label="Filter by Year" options={years} onSelect={onYearSelect} />
    <Button onClick={onSortChange} icon="sort">
      Sort
      {sort === 'asc' ? ' ascending' : sort === 'desc' ? ' descending' : ''}
    </Button>
  </div>
)
