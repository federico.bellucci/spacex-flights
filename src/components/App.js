import React, { useState, useEffect } from 'react'
import axios from 'axios'

import './App.scss'
import { getLaunches, getYears } from '../api'
import { IDLE, LOADING, ERROR } from './status'
import Header from '../layout/Header'
import Launches from './Launches'

function App () {
  const [data, setData] = useState({
    launches: [],
    years: []
  })
  const [status, setStatus] = useState(IDLE)
  const [year, setYear] = useState(null)
  const [sort, setSort] = useState(null)

  useEffect(() => {
    refresh()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [year, sort])

  const onSortChange = () => {
    let newSort
    switch (sort) {
      case 'asc':
        newSort = 'desc'
        break
      case 'desc':
        newSort = null
        break
      default:
        newSort = 'asc'
    }
    setSort(newSort)
  }

  const refresh = () => {
    setStatus(LOADING)
    Promise.all([getLaunches({ year, sort }), getYears()])
      .then(([launches, years]) => {
        setStatus(IDLE)
        setData({ launches, years })
      })
      .catch((error) => {
        if (axios.isCancel(error)) {
          console.log('Years request canceled.', error.message)
        } else {
          setStatus(ERROR)
          console.log(error.message)
        }
      })
  }

  return (
    <div className="app">
      <Header refresh={refresh} />
      <Launches
        data={data}
        status={status}
        year={year}
        onYearSelect={setYear}
        sort={sort}
        onSortChange={onSortChange}
      />
    </div>
  )
}

export default App
