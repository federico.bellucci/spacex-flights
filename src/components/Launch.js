import React from 'react'
import './Launch.scss'

export default ({
  item: {
    id, missionName, rocketName, date
  }
}) => (
  <div className="list-item">
    <div className="list-item--index">{`#${id}`}</div>
    <div className="list-item--main">{missionName}</div>
    <div className="list-item--side">
      <div className="list-item--side--top">{date}</div>
      <div className="list-item--side--bottom">{rocketName}</div>
    </div>
  </div>
)
